#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
{Description}

License:
BSD-3-Clause
"""

# Futures

# Built-in/Generic Imports

# Libs

# Own modules

__author__ = 'Jacob M Olson'
__copyright__ = 'Copyright 2021, Jacob M Olson'
__credits__ = ['Jacob M Olson']
__license__ = 'BSD-3-Clause'
__version__ = '0.0.1'
__maintainer__ = 'Jacob M Olson'
__email__ = 'jolson1129@gmail.com'
__status__ = 'dev'
